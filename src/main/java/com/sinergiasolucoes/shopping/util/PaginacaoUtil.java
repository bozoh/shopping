package com.sinergiasolucoes.shopping.util;

/**
 * Classe Utilitária para ajudar na paginação
 * @author carlosalexandre
 *
 */
public class PaginacaoUtil {

	/**
	 * Calcula a posição inicial baseado na página atual e 
	 * a quantidade de valores por página
	 * 
	 * @param pagina página atual
	 * @param porPagina quantidade de valores por página
	 * @return posição inicial calculada
	 */
	public static Integer getPosicaoInicial(Integer pagina, Integer porPagina) {
		if (pagina == null || porPagina == null)
			return null;

		return Integer.valueOf((pagina * porPagina - porPagina) + 1);

	}

	
}

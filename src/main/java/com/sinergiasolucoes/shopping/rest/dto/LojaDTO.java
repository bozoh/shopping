package com.sinergiasolucoes.shopping.rest.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import javax.xml.bind.annotation.XmlRootElement;

import com.sinergiasolucoes.shopping.model.Loja;

@XmlRootElement
public class LojaDTO implements Serializable {

	private static final long serialVersionUID = -1195479009059294718L;

	private Long id;
	private String nome;
	private String andar;
	private String numero;
	private String site;
	private String facebook;
	private String twitter;
	private String descricao;
	private Set<SegmentoDTO> segmentos = new HashSet<SegmentoDTO>();
	private Date dataInicio;
	private Date dataFim;
	private String logotipo;
	private Integer likes;
	private String CNPJ;

	public LojaDTO() {
	}

	public static LojaDTO toNestedLojaDTO(Loja entity) {
		if (entity == null)
			return null;

		LojaDTO dto = new LojaDTO();
		dto.id = entity.getId();
		dto.nome = entity.getNome();
		dto.andar = entity.getAndar();
		dto.numero = entity.getNumero();
		dto.site = entity.getSite();
		dto.facebook = entity.getFacebook();
		dto.twitter = entity.getTwitter();
		dto.descricao = entity.getDescricao();
		dto.dataInicio = entity.getDataInicio();
		dto.dataFim = entity.getDataFim();
		dto.logotipo = entity.getLogotipo();
		dto.likes = entity.getLikes();
		dto.CNPJ = entity.getCNPJ();

		return dto;
	}

	public static Set<LojaDTO> toNestedLojaDTOCollection(Set<Loja> entities) {
		if (entities == null)
			return new HashSet<>();

		return entities.stream().map(l -> {
			return LojaDTO.toNestedLojaDTO(l);
		}).collect(Collectors.toSet());

	}

	public LojaDTO(final Loja entity) {
		if (entity != null) {
			this.id = entity.getId();
			this.nome = entity.getNome();
			this.andar = entity.getAndar();
			this.numero = entity.getNumero();
			this.site = entity.getSite();
			this.facebook = entity.getFacebook();
			this.twitter = entity.getTwitter();
			this.descricao = entity.getDescricao();
			this.segmentos = SegmentoDTO.toNestedSegmentoDTOCollection(entity.getSegmentos());
			
			
			this.dataInicio = entity.getDataInicio();
			this.dataFim = entity.getDataFim();
			this.logotipo = entity.getLogotipo();
			this.likes = entity.getLikes();
			this.CNPJ = entity.getCNPJ();
		}
	}

	
	public Long getId() {
		return this.id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(final String nome) {
		this.nome = nome;
	}

	public String getAndar() {
		return this.andar;
	}

	public void setAndar(final String andar) {
		this.andar = andar;
	}

	public String getNumero() {
		return this.numero;
	}

	public void setNumero(final String numero) {
		this.numero = numero;
	}

	public String getSite() {
		return this.site;
	}

	public void setSite(final String site) {
		this.site = site;
	}

	public String getFacebook() {
		return this.facebook;
	}

	public void setFacebook(final String facebook) {
		this.facebook = facebook;
	}

	public String getTwitter() {
		return this.twitter;
	}

	public void setTwitter(final String twitter) {
		this.twitter = twitter;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public void setDescricao(final String descricao) {
		this.descricao = descricao;
	}

	public Set<SegmentoDTO> getSegmentos() {
		return this.segmentos;
	}

	public void setSegmentos(final Set<SegmentoDTO> segmentos) {
		this.segmentos = segmentos;
	}

	public Date getDataInicio() {
		return this.dataInicio;
	}

	public void setDataInicio(final Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Date getDataFim() {
		return this.dataFim;
	}

	public void setDataFim(final Date dataFim) {
		this.dataFim = dataFim;
	}

	public String getLogotipo() {
		return this.logotipo;
	}

	public void setLogotipo(final String logotipo) {
		this.logotipo = logotipo;
	}

	public Integer getLikes() {
		return this.likes;
	}

	public void setLikes(final Integer likes) {
		this.likes = likes;
	}

	public String getCNPJ() {
		return this.CNPJ;
	}

	public void setCNPJ(final String CNPJ) {
		this.CNPJ = CNPJ;
	}
}
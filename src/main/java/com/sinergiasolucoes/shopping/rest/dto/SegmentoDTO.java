package com.sinergiasolucoes.shopping.rest.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import javax.xml.bind.annotation.XmlRootElement;

import com.sinergiasolucoes.shopping.model.Segmento;

@XmlRootElement
public class SegmentoDTO implements Serializable {

	private static final long serialVersionUID = -5713022130020052183L;
	private Long id;
	private String nome;
	private Set<LojaDTO> lojas = new HashSet<LojaDTO>();

	public SegmentoDTO() {
	}

	public SegmentoDTO(final Segmento entity) {
		if (entity != null) {
			this.id = entity.getId();
			this.nome = entity.getNome();
		}
	}

	public static SegmentoDTO toNestedSegmentoDTO(Segmento entity) {
		if (entity == null)
			return null;
		
		SegmentoDTO dto = new SegmentoDTO();
		dto.setId(entity.getId());
		dto.setNome(entity.getNome());
		return dto;
	}

	public static Set<SegmentoDTO> toNestedSegmentoDTOCollection(Set<Segmento> entities) {
		if (entities == null) 
			return new HashSet<>();
		
		return entities.stream().map( s -> {
			return SegmentoDTO.toNestedSegmentoDTO(s);
		}).collect(Collectors.toSet());
	}

	public Long getId() {
		return this.id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(final String nome) {
		this.nome = nome;
	}

	public Set<LojaDTO> getLojas() {
		return this.lojas;
	}

	public void setLojas(final Set<LojaDTO> lojas) {
		this.lojas = lojas;
	}
}
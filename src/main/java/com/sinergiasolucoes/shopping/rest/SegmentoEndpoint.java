package com.sinergiasolucoes.shopping.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.sinergiasolucoes.shopping.model.Segmento;
import com.sinergiasolucoes.shopping.rest.dto.SegmentoDTO;
import com.sinergiasolucoes.shopping.service.SegmentoService;

/**
 * 
 */
@Stateless
@Path("/segmentos")
public class SegmentoEndpoint {

	@Inject
	private SegmentoService service;

	@GET
	@Path("/{id:[0-9][0-9]*}")
	@Produces("application/json")
	public Response findById(@PathParam("id") Long id) {
		Segmento entity = service.findById(id);
		if (entity == null)
			return Response.status(Status.NOT_FOUND).build();

		return Response.ok(new SegmentoDTO(entity)).build();
	}

	@GET
	@Produces("application/json")
	public List<SegmentoDTO> listAll(@QueryParam("pagina") Integer pagina, @QueryParam("porpagina") Integer porPagina) {

		List<Segmento> searchResults = service.findAll(pagina, porPagina);
		if (searchResults == null)
			searchResults = new ArrayList<Segmento>();

		return searchResults.stream().map(s -> {
			return new SegmentoDTO(s);
		}).collect(Collectors.toList());
	}

}

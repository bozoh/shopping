package com.sinergiasolucoes.shopping.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.OptimisticLockException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;

import com.sinergiasolucoes.shopping.model.Loja;
import com.sinergiasolucoes.shopping.rest.dto.LojaDTO;
import com.sinergiasolucoes.shopping.service.LojaService;
import com.sinergiasolucoes.shopping.service.SegmentoService;

/**
 * 
 */
@Stateless
@Path("/lojas")
public class LojaEndpoint {

	@Inject
	private LojaService servicoLoja;

	@Inject
	private SegmentoService servicoSegmento;

	@POST
	@Consumes("application/json")
	public Response create(LojaDTO dto) {
		Loja entity = toEntity(dto, null);
		servicoLoja.create(entity);
		return Response
				.created(UriBuilder.fromResource(LojaEndpoint.class).path(String.valueOf(entity.getId())).build())
				.build();
	}

	@DELETE
	@Path("/{id:[0-9][0-9]*}")
	public Response deleteById(@PathParam("id") Long id) {
		Loja entity = servicoLoja.findById(id);
		if (entity == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		servicoLoja.delete(entity);
		return Response.noContent().build();
	}

	@GET
	@Path("/{id:[0-9][0-9]*}")
	@Produces("application/json")
	public Response findById(@PathParam("id") Long id) {
		Loja entity = servicoLoja.findById(id);
		if (entity == null) {
			return Response.status(Status.NOT_FOUND).build();
		}

		LojaDTO dto = new LojaDTO(entity);
		return Response.ok(dto).build();
	}

	@GET
	@Produces("application/json")
	public List<LojaDTO> listAll(@QueryParam("pagina") Integer pagina,
			@QueryParam("porpagina") Integer resultadosPorPagina) {

		return toDTOCollection(servicoLoja.listAll(pagina, resultadosPorPagina));

	}

	@PUT
	@Path("/{id:[0-9][0-9]*}")
	@Consumes("application/json")
	public Response update(@PathParam("id") Long id, LojaDTO dto) {
		if (dto == null) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		if (id == null) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		if (!id.equals(dto.getId())) {
			return Response.status(Status.CONFLICT).entity(dto).build();
		}
		Loja entity = servicoLoja.findById(id);
		if (entity == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		entity = toEntity(dto, entity);
		try {
			entity = servicoLoja.update(entity);
		} catch (OptimisticLockException e) {
			return Response.status(Status.CONFLICT).entity(e.getEntity()).build();
		}
		return Response.noContent().build();
	}

	private List<LojaDTO> toDTOCollection(List<Loja> entities) {
		if (entities == null)
			return new ArrayList<>();
		return entities.stream().map(l -> {
			return new LojaDTO(l);
		}).collect(Collectors.toList());
	}

	private Loja toEntity(LojaDTO dto, Loja entity) {

		if (entity == null) {
			entity = new Loja();
		}
		entity.setNome(dto.getNome());
		entity.setAndar(dto.getAndar());
		entity.setNumero(dto.getNumero());
		entity.setSite(dto.getSite());
		entity.setFacebook(dto.getFacebook());
		entity.setTwitter(dto.getTwitter());
		entity.setDescricao(dto.getDescricao());
		entity.setDataInicio(dto.getDataInicio());
		entity.setDataFim(dto.getDataFim());
		entity.setLogotipo(dto.getLogotipo());
		entity.setLikes(dto.getLikes());
		entity.setCNPJ(dto.getCNPJ());

//		Set<Segmento> toRemove = entity.getSegmentos();
//		Set<SegmentoDTO> toAdd = dto.getSegmentos();
//		
//		toRemove.removeAll(dto.getSegmentos());
//		toAdd.removeAll(entity.getSegmentos());
//		
//		entity.getSegmentos().removeAll(toRemove);
		entity.setSegmentos(dto.getSegmentos().stream().map((segmentoDto) -> {
			return servicoSegmento.findById(segmentoDto.getId());
		}).collect(Collectors.toSet()));

		return entity;
	}

}

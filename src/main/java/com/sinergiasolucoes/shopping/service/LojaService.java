package com.sinergiasolucoes.shopping.service;

import java.io.Serializable;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

import com.sinergiasolucoes.shopping.dao.LojaDao;
import com.sinergiasolucoes.shopping.model.Loja;
import com.sinergiasolucoes.shopping.model.Segmento;
import com.sinergiasolucoes.shopping.util.PaginacaoUtil;

@Stateless
@LocalBean
public class LojaService implements Serializable {

	private static final long serialVersionUID = 4640406785527063949L;
	@Inject
	private LojaDao dao;

	public void create(Loja l) {
		dao.create(l);
	}

	public Loja findById(Long id) {
		return dao.findById(id);
	}

	public List<Loja> findByNome(String nome, Integer pagina, Integer resultadosPorPagina) {
		Integer posicaoInicial = PaginacaoUtil.getPosicaoInicial(pagina, resultadosPorPagina);
		return dao.findBynome(nome, posicaoInicial, resultadosPorPagina);
	}

	public Loja update(Loja l) {
		return dao.update(l);
	}

	public void delete(Long id) {
		dao.deleteById(id);
	}

	public void delete(Loja l) {
		this.delete(l.getId());
	}

	public List<Loja> listAll(Integer pagina, Integer resultadosPorPagina) {
		Integer posicaoInicial = PaginacaoUtil.getPosicaoInicial(pagina, resultadosPorPagina);
		return dao.listAll(posicaoInicial, resultadosPorPagina);

	}

	public List<Loja> listAllAtivas(Integer pagina, Integer resultadosPorPagina) {
		Integer posicaoInicial = PaginacaoUtil.getPosicaoInicial(pagina, resultadosPorPagina);
		return dao.listAllAtivas(posicaoInicial, resultadosPorPagina);
	}

	public List<Loja> listAllSegmentos(List<Segmento> segmentos, Integer pagina, Integer resultadosPorPagina) {
		Integer posicaoInicial = PaginacaoUtil.getPosicaoInicial(pagina, resultadosPorPagina);
		return dao.listAllSegmentos(segmentos, posicaoInicial, resultadosPorPagina);
	}

}
package com.sinergiasolucoes.shopping.service;

import java.io.Serializable;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

import com.sinergiasolucoes.shopping.dao.SegmentoDao;
import com.sinergiasolucoes.shopping.model.Segmento;
import com.sinergiasolucoes.shopping.util.PaginacaoUtil;

@Stateless
@LocalBean
public class SegmentoService implements Serializable {

	private static final long serialVersionUID = 7185996798909607745L;
	
	@Inject
	private SegmentoDao dao;

	public Segmento findById(Long id) {
		return dao.findById(id);
	}

	public List<Segmento> findAll(Integer pagina, Integer resultadosPorPagina) {
		Integer posicaoInicial = PaginacaoUtil.getPosicaoInicial(pagina, resultadosPorPagina);
		return dao.listAll(posicaoInicial, resultadosPorPagina);
	}
}
package com.sinergiasolucoes.shopping.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.validation.constraints.Size;
import com.sinergiasolucoes.shopping.constraints.CNPJ;
import com.sinergiasolucoes.shopping.rest.dto.LojaDTO;
import com.sinergiasolucoes.shopping.model.Segmento;

@Entity
public class Loja implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", updatable = false, nullable = false)
	private Long id;
	private static final long serialVersionUID = 1L;
	@Version
	@Column(name = "version")
	private int version;

	@Column(nullable = false)
	@NotNull
	@Size(min = 4, max = 255)
	private String nome;

	@Column(length = 14, name = "cnpj", nullable = false)
	@NotNull
	@Size(min = 14, max = 14)
	@CNPJ
	private String CNPJ;

	@Column(nullable = false)
	@NotNull
	private String andar;

	@Column(nullable = false)
	@NotNull
	private String numero;

	@Column(nullable = false, columnDefinition = "VARCHAR(2083) DEFAULT ''")
	@Size(max = 2083)
	@Null
	private String site;

	@Column(nullable = false, columnDefinition = "VARCHAR(255) DEFAULT ''")
	@Size(max = 255)
	@Null
	private String facebook;

	@Column(nullable = false, columnDefinition = "VARCHAR(255) DEFAULT ''")
	@Size(max = 255)
	@Null
	private String twitter;

	@Column(nullable = false, columnDefinition = "VARCHAR(1000) DEFAULT ''")
	@Size(max = 1000)
	@Null
	private String descricao;

	@Column(name = "data_inicio", nullable = false)
	@Temporal(TemporalType.DATE)
	@NotNull
	private Date dataInicio;

	@Column(name = "data_fim")
	@Temporal(TemporalType.DATE)
	@Null
	private Date dataFim;

	@Column(nullable = false, columnDefinition = "VARCHAR(255) DEFAULT ''")
	@Null
	private String logotipo;

	@Column(nullable = false, columnDefinition = "INT DEFAULT 0")
	private Integer likes;

	@ManyToMany
	private Set<Segmento> segmentos = new HashSet<Segmento>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Loja) && !(obj instanceof LojaDTO)) {
			return false;
		}
		Long otherId = null;
		if (obj instanceof Loja) {
			otherId = ((Loja) obj).getId();
		} else {
			otherId = ((LojaDTO) obj).getId();
		}

		if (id != null) {
			if (!id.equals(otherId)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCNPJ() {
		return CNPJ;
	}

	public void setCNPJ(String CNPJ) {
		this.CNPJ = CNPJ;
	}

	public String getAndar() {
		return andar;
	}

	public void setAndar(String andar) {
		this.andar = andar;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public String getFacebook() {
		return facebook;
	}

	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}

	public String getTwitter() {
		return twitter;
	}

	public void setTwitter(String twitter) {
		this.twitter = twitter;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	// public void addSegmento(Segmento segmento) {
	// this.segmentos.add(segmento);
	// segmento.addLoja(this);
	// }
	//
	// public void removeSegmento(Segmento segmento) {
	// this.segmentos.remove(segmento);
	// segmento.removeLoja(this);
	// }

	public String getLogotipo() {
		return logotipo;
	}

	public void setLogotipo(String logotipo) {
		this.logotipo = logotipo;
	}

	public Integer getLikes() {
		return likes;
	}

	public void setLikes(Integer likes) {
		this.likes = likes;
	}

	@Override
	public String toString() {
		String result = getClass().getSimpleName() + " ";
		if (nome != null && !nome.trim().isEmpty())
			result += "nome: " + nome;
		if (CNPJ != null && !CNPJ.trim().isEmpty())
			result += ", CNPJ: " + CNPJ;
		if (andar != null && !andar.trim().isEmpty())
			result += ", andar: " + andar;
		if (numero != null && !numero.trim().isEmpty())
			result += ", numero: " + numero;
		if (site != null && !site.trim().isEmpty())
			result += ", site: " + site;
		if (facebook != null && !facebook.trim().isEmpty())
			result += ", facebook: " + facebook;
		if (twitter != null && !twitter.trim().isEmpty())
			result += ", twitter: " + twitter;
		if (descricao != null && !descricao.trim().isEmpty())
			result += ", descricao: " + descricao;
		if (logotipo != null && !logotipo.trim().isEmpty())
			result += ", logotipo: " + logotipo;
		if (likes != null)
			result += ", likes: " + likes;
		return result;
	}

	public Set<Segmento> getSegmentos() {
		return this.segmentos;
	}

	public void setSegmentos(final Set<Segmento> segmentos) {
		this.segmentos = segmentos;
	}
}
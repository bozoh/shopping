package com.sinergiasolucoes.shopping.dao;

import java.util.Collection;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.sinergiasolucoes.shopping.model.Loja;
import com.sinergiasolucoes.shopping.model.Segmento;

/**
 * DAO for Loja
 */
@Stateless
public class LojaDao {
	@PersistenceContext(unitName = "shopping-persistence-unit")
	EntityManager em;

	public void create(Loja entity) {
		em.persist(entity);
	}

	public void deleteById(Long id) {
		Loja entity = em.find(Loja.class, id);
		if (entity != null) {
			em.remove(entity);
		}
	}

	public Loja findById(Long id) {
		return em.find(Loja.class, id);
	}

	public Loja update(Loja entity) {
		return em.merge(entity);
	}

	public List<Loja> listAll(Integer startPosition, Integer maxResult) {
		TypedQuery<Loja> findAllQuery = em
				.createQuery("SELECT DISTINCT l FROM Loja l LEFT JOIN FETCH l.segmentos ORDER BY l.nome", Loja.class);
		if (startPosition != null) {
			findAllQuery.setFirstResult(startPosition);
		}
		if (maxResult != null) {
			findAllQuery.setMaxResults(maxResult);
		}
		return findAllQuery.getResultList();
	}

	/**
	 * Retorna todas as lojas ativas, sem uma data de fim
	 * 
	 * @param startPosition
	 * @param maxResult
	 * @return
	 */
	public List<Loja> listAllAtivas(Integer startPosition, Integer maxResult) {
		TypedQuery<Loja> findAllQuery = em.createQuery(
				"SELECT DISTINCT l FROM Loja l LEFT JOIN FETCH l.segmentos WHERE l.dataFim IS NULL ORDER BY l.nome",
				Loja.class);
		if (startPosition != null) {
			findAllQuery.setFirstResult(startPosition);
		}
		if (maxResult != null) {
			findAllQuery.setMaxResults(maxResult);
		}
		return findAllQuery.getResultList();
	}

	/**
	 * Retorna todas as lojas ativas, sem uma data de fim de uma lista de segmentos
	 * 
	 * @param segmentos     Segmentos das lojas
	 * @param startPosition
	 * @param maxResult
	 * @return
	 */
	public List<Loja> listAllSegmentos(Collection<Segmento> segmentos, Integer startPosition, Integer maxResult) {
		TypedQuery<Loja> query = em.createQuery(
				"SELECT DISTINCT l FROM Loja l LEFT JOIN FETCH l.segmentos s WHERE s IN (:segmentos) "
				+ "AND l.dataFim IS NULL "
				+ "ORDER BY l.nome", Loja.class);
		query.setParameter("segmentos", segmentos);

		if (startPosition != null) {
			query.setFirstResult(startPosition);
		}
		if (maxResult != null) {
			query.setMaxResults(maxResult);
		}
		return query.getResultList();
	}

	/**
	 * Retorna todas as lojas ativas, sem uma data de fim por nome
	 * 
	 * @nome Nome da loja
	 * @param startPosition
	 * @param maxResult
	 * @return
	 */
	public List<Loja> findBynome(String nome, Integer startPosition, Integer maxResult) {
		TypedQuery<Loja> query = em.createQuery(
				"SELECT DISTINCT l FROM Loja l LEFT JOIN FETCH l.segmentos s WHERE lower(l.nome) like lower(concat('%', :nome,'%')) "
				+ "AND l.dataFim IS NULL " 
				+ "ORDER BY l.nome", Loja.class);
		query.setParameter("nome", nome);

		if (startPosition != null) {
			query.setFirstResult(startPosition);
		}
		if (maxResult != null) {
			query.setMaxResults(maxResult);
		}
		return query.getResultList();
	}
}

package com.sinergiasolucoes.shopping.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import com.sinergiasolucoes.shopping.model.Segmento;

/**
 * DAO for Segmento
 */
@Stateless
public class SegmentoDao {
	@PersistenceContext(unitName = "shopping-persistence-unit")
	EntityManager em;

	public void create(Segmento entity) {
		em.persist(entity);
	}

	public void deleteById(Long id) {
		Segmento entity = em.find(Segmento.class, id);
		if (entity != null) {
			em.remove(entity);
		}
	}

	public Segmento findById(Long id) {
		return em.find(Segmento.class, id);
	}

	public Segmento update(Segmento entity) {
		return em.merge(entity);
	}

	public List<Segmento> listAll(Integer startPosition, Integer maxResult) {
		TypedQuery<Segmento> findAllQuery = em
				.createQuery(
						"SELECT DISTINCT s FROM Segmento s LEFT JOIN FETCH s.lojas ORDER BY s.nome",
						Segmento.class);
		if (startPosition != null) {
			findAllQuery.setFirstResult(startPosition);
		}
		if (maxResult != null) {
			findAllQuery.setMaxResults(maxResult);
		}
		return findAllQuery.getResultList();
	}
}

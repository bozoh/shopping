package com.sinergiasolucoes.shopping.constraints;

import javax.validation.Constraint;
import javax.validation.ReportAsSingleViolation;

import com.sinergiasolucoes.shopping.constraints.validator.CNPJValitador;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.annotation.ElementType;
import java.lang.annotation.Documented;
import javax.validation.Payload;

@Constraint(validatedBy = CNPJValitador.class)
@ReportAsSingleViolation
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER,
		ElementType.TYPE, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR})
@Documented
public @interface CNPJ {

	String message() default "CNPJ inválido";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

	@Retention(RetentionPolicy.RUNTIME)
	@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER,
			ElementType.TYPE, ElementType.ANNOTATION_TYPE,
			ElementType.CONSTRUCTOR})
	public @interface List {
		CNPJ[] value();
	}
}
package com.sinergiasolucoes.shopping.constraints.validator;

import static org.assertj.core.api.Assertions.assertThat;

import javax.validation.ConstraintValidatorContext;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import junit.framework.TestCase;

public class CNPJValitadorTest extends TestCase {

	CNPJValitador validator;
	
	@Mock
	ConstraintValidatorContext context;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		this.validator = new CNPJValitador();
	}
	
	@Test
	public void teste_isValid() {
		
		//False para nulo e em branco
		assertThat(validator.isValid(null, context)).isFalse();
		assertThat(validator.isValid("", context)).isFalse();
		
		
		//False para menor que 14
		assertThat(validator.isValid("1", context)).isFalse();
		assertThat(validator.isValid("12", context)).isFalse();
		assertThat(validator.isValid("123", context)).isFalse();
		assertThat(validator.isValid("1234", context)).isFalse();
		assertThat(validator.isValid("12345", context)).isFalse();
		assertThat(validator.isValid("123456", context)).isFalse();
		assertThat(validator.isValid("1234567", context)).isFalse();
		assertThat(validator.isValid("12345678", context)).isFalse();
		assertThat(validator.isValid("123456789", context)).isFalse();
		assertThat(validator.isValid("1234567890", context)).isFalse();
		assertThat(validator.isValid("12345678901", context)).isFalse();
		assertThat(validator.isValid("123456789012", context)).isFalse();
		assertThat(validator.isValid("1234567890123", context)).isFalse();
		
		//False com letras ou outros caratecers não numéricos
		assertThat(validator.isValid("71893336000179", context)).isTrue();
		assertThat(validator.isValid("7189333600017E", context)).isFalse();
		assertThat(validator.isValid("718933 6000179", context)).isFalse();
		assertThat(validator.isValid("718933360(0179", context)).isFalse();
		assertThat(validator.isValid("71893#36000179", context)).isFalse();
		
		
		//TRUE válidos com formatação
		
		assertThat(validator.isValid("76.981.800/0001-00", context)).isTrue();
		assertThat(validator.isValid("62.223.556/0001-02", context)).isTrue();
		assertThat(validator.isValid("25.810.352/0001-33", context)).isTrue();
		
		//TRUE válidos sem formatação
		assertThat(validator.isValid("71893336000179", context)).isTrue();
		assertThat(validator.isValid("56864232000142", context)).isTrue();
		assertThat(validator.isValid("78073895000181", context)).isTrue();
		
		//False com todos os números iguais
		assertThat(validator.isValid("00000000000000", context)).isFalse();
		assertThat(validator.isValid("11111111111111", context)).isFalse();
		assertThat(validator.isValid("22222222222222", context)).isFalse();
		assertThat(validator.isValid("33333333333333", context)).isFalse();
		assertThat(validator.isValid("44444444444444", context)).isFalse();
		assertThat(validator.isValid("55555555555555", context)).isFalse();
		assertThat(validator.isValid("66666666666666", context)).isFalse();
		assertThat(validator.isValid("77777777777777", context)).isFalse();
		assertThat(validator.isValid("88888888888888", context)).isFalse();
		assertThat(validator.isValid("99999999999999", context)).isFalse();
	}
}

package com.sinergiasolucoes.shopping.dao;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.sinergiasolucoes.shopping.model.Loja;
import com.sinergiasolucoes.shopping.model.Segmento;

import junit.framework.TestCase;

public class LojaDaoUnitTest extends TestCase{
	
	@Mock
	private EntityManager em;
	
	private LojaDao lDao;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		this.lDao = new LojaDao();
		this.lDao.em = em;
	}
	
	@Test
	public void teste_findById() {
		lDao.findById(2l);
		verify(em, times(1)).find(Loja.class, 2l);
		
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void teste_findByNome() {
		TypedQuery mockedQuery = mock(TypedQuery.class);
		when(em.createQuery(anyString(), any())).thenReturn(mockedQuery);
		
		//Sem paginação
		lDao.findBynome("teste sem paginação", null, null);
		verify(mockedQuery, atLeastOnce()).setParameter("nome", "teste sem paginação");
		verify(mockedQuery, never()).setFirstResult(anyInt());
		verify(mockedQuery, never()).setMaxResults(anyInt());
		
		
		//Com paginação
		lDao.findBynome("teste com paginação", 1, 10);
		verify(mockedQuery, atLeastOnce()).setParameter("nome", "teste com paginação");
		verify(mockedQuery, times(1)).setFirstResult(1);
		verify(mockedQuery, times(1)).setMaxResults(10);

				
		verify(em, times(2)).createQuery("SELECT DISTINCT l FROM Loja l "
				+ "LEFT JOIN FETCH l.segmentos s "
				+ "WHERE lower(l.nome) like lower(concat('%', :nome,'%')) "
				+ "AND l.dataFim IS NULL "
				+ "ORDER BY l.nome", Loja.class);
		
		verify(mockedQuery, times(2)).getResultList();
		
	}
	

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void teste_listAll() {
		TypedQuery mockedQuery = mock(TypedQuery.class);
		when(em.createQuery(anyString(), any())).thenReturn(mockedQuery);
		
		//Sem paginação
		lDao.listAll(null, null);
		verify(mockedQuery, never()).setFirstResult(anyInt());
		verify(mockedQuery, never()).setMaxResults(anyInt());
				
		//Com paginação
		lDao.listAll(1, 10);
		verify(mockedQuery, times(1)).setFirstResult(1);
		verify(mockedQuery, times(1)).setMaxResults(10);
				
		verify(em, times(2)).createQuery("SELECT DISTINCT l FROM Loja l LEFT JOIN FETCH l.segmentos ORDER BY l.nome", Loja.class);
		verify(mockedQuery, times(2)).getResultList();
		
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void teste_listAll_com_paginacao() {
		TypedQuery mockedQuery = mock(TypedQuery.class);
		when(em.createQuery(anyString(), any())).thenReturn(mockedQuery);
		lDao.listAll(1, 10);
		verify(em, times(1)).createQuery("SELECT DISTINCT l FROM Loja l LEFT JOIN FETCH l.segmentos ORDER BY l.nome", Loja.class);
	
		verify(mockedQuery, times(1)).setFirstResult(1);
		verify(mockedQuery, times(1)).setMaxResults(10);
		verify(mockedQuery, times(1)).getResultList();
		
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void teste_listAllSegmentos() {
		TypedQuery mockedQuery = mock(TypedQuery.class);
		
		ArrayList<Segmento> segmentos = new ArrayList<>();
		segmentos.add(new Segmento());
		segmentos.add(new Segmento());
		segmentos.add(new Segmento());
		
		when(em.createQuery(anyString(), any())).thenReturn(mockedQuery);
		
		//Sem paginação
		lDao.listAllSegmentos(segmentos, null, null);
		verify(mockedQuery, atLeastOnce()).setParameter("segmentos", segmentos);
		verify(mockedQuery, never()).setFirstResult(anyInt());
		verify(mockedQuery, never()).setMaxResults(anyInt());
		
		
		//Com paginação
		lDao.listAllSegmentos(segmentos, 1, 10);
		verify(mockedQuery, atLeastOnce()).setParameter("segmentos", segmentos);
		verify(mockedQuery, times(1)).setFirstResult(1);
		verify(mockedQuery, times(1)).setMaxResults(10);

				
		verify(em, times(2)).createQuery("SELECT DISTINCT l FROM Loja l "
				+ "LEFT JOIN FETCH l.segmentos s "
				+ "WHERE s IN (:segmentos) AND "
				+ "l.dataFim IS NULL "
				+ "ORDER BY l.nome", Loja.class);
		
		verify(mockedQuery, times(2)).getResultList();
		
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void teste_listAllAtivas() {
		TypedQuery mockedQuery = mock(TypedQuery.class);
		when(em.createQuery(anyString(), any())).thenReturn(mockedQuery);
		
		//Sem paginação
		lDao.listAllAtivas(null, null);
		verify(mockedQuery, never()).setFirstResult(anyInt());
		verify(mockedQuery, never()).setMaxResults(anyInt());
				
		//Com paginação
		lDao.listAllAtivas(1, 10);
		verify(mockedQuery, times(1)).setFirstResult(1);
		verify(mockedQuery, times(1)).setMaxResults(10);
				
		verify(em, times(2)).createQuery("SELECT DISTINCT l FROM Loja l LEFT JOIN FETCH l.segmentos WHERE l.dataFim IS NULL ORDER BY l.nome", Loja.class);
		verify(mockedQuery, times(2)).getResultList();
		
	}
	
}

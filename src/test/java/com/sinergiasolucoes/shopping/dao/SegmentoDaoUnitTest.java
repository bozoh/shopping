package com.sinergiasolucoes.shopping.dao;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.sinergiasolucoes.shopping.model.Segmento;

import junit.framework.TestCase;

public class SegmentoDaoUnitTest extends TestCase{
	
	@Mock
	private EntityManager em;
	
	private SegmentoDao sDao;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		this.sDao = new SegmentoDao();
		this.sDao.em = em;
	}
	
	@Test
	public void teste_findById() {
		sDao.findById(2l);
		verify(em, times(1)).find(Segmento.class, 2l);
		
	}
	

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void teste_listAll() {
		TypedQuery mockedQuery = mock(TypedQuery.class);
		when(em.createQuery(anyString(), any())).thenReturn(mockedQuery);
		sDao.listAll(null, null);
		verify(em, times(1)).createQuery("SELECT DISTINCT s FROM Segmento s LEFT JOIN FETCH s.lojas ORDER BY s.nome", Segmento.class);
		verify(mockedQuery, times(1)).getResultList();
		
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void teste_listAll_com_paginacao() {
		TypedQuery mockedQuery = mock(TypedQuery.class);
		when(em.createQuery(anyString(), any())).thenReturn(mockedQuery);
		sDao.listAll(1, 10);
		verify(em, times(1)).createQuery("SELECT DISTINCT s FROM Segmento s LEFT JOIN FETCH s.lojas ORDER BY s.nome", Segmento.class);
	
		verify(mockedQuery, times(1)).setFirstResult(1);
		verify(mockedQuery, times(1)).setMaxResults(10);
		verify(mockedQuery, times(1)).getResultList();
		
	}
}
